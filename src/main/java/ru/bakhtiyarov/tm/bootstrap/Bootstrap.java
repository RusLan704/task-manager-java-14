package ru.bakhtiyarov.tm.bootstrap;

import ru.bakhtiyarov.tm.api.controller.*;
import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.api.service.*;
import ru.bakhtiyarov.tm.constant.ArgumentConst;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.controller.*;
import ru.bakhtiyarov.tm.exception.system.IncorrectArgumentException;
import ru.bakhtiyarov.tm.exception.system.IncorrectCommandException;
import ru.bakhtiyarov.tm.repository.CommandRepository;
import ru.bakhtiyarov.tm.repository.ProjectRepository;
import ru.bakhtiyarov.tm.repository.TaskRepository;
import ru.bakhtiyarov.tm.repository.UserRepository;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.service.*;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final IUserController userController = new UserController(userService,authService);

    private final IAuthController authController = new AuthController(authService);


    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);


    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authService);


    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authService);

    private void initUsers(){
        userService.create("test","test","test@test.ru");
        userService.create("admin","admin", Role.ADMIN);
    }

    public void run(final String... args) {
        commandController.displayWelcome();
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean parseArgs(final String... args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default: throw new IncorrectArgumentException(arg);
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.LOGIN:
                authController.login();
                break;
            case TerminalConst.LOGOUT:
                authController.logout();
                break;
            case TerminalConst.REGISTRY:
                authController.registry();
                break;
            case TerminalConst.USER_VIEW_PROFILE:
                userController.showUserProfile();
                break;
            case TerminalConst.USER_UPDATE_PASSWORD:
                userController.updateUserPassword();
                break;
            case TerminalConst.USER_UPDATE_LOGIN:
                userController.updateUserLogin();
                break;
            case TerminalConst.USER_UPDATE_FIRST_NAME:
                userController.updateUserFirstName();
                break;
            case TerminalConst.USER_UPDATE_LAST_NAME:
                userController.updateUserLastName();
                break;
            case TerminalConst.USER_UPDATE_MIDDLE_NAME:
                userController.updateUserMiddleName();
                break;
            case TerminalConst.USER_UPDATE_EMAIL:
                userController.updateUserEmail();
                break;
            default:
                throw new IncorrectCommandException(arg);
        }
    }

}
