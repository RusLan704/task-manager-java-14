package ru.bakhtiyarov.tm.exception.empty;

public class EmptyFirstNameException extends RuntimeException {

    public EmptyFirstNameException() {
        super("Error! First name is empty...");
    }

}