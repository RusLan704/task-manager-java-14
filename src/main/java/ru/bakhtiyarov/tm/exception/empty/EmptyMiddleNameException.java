package ru.bakhtiyarov.tm.exception.empty;

public class EmptyMiddleNameException extends RuntimeException {

    public EmptyMiddleNameException() {
        super("Error! Middle name is empty...");
    }

}