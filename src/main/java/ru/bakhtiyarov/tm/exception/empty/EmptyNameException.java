package ru.bakhtiyarov.tm.exception.empty;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
