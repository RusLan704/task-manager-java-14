package ru.bakhtiyarov.tm.exception.empty;

public class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
