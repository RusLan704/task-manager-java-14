package ru.bakhtiyarov.tm.exception.empty;

public class EmptyEmailException extends RuntimeException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}
