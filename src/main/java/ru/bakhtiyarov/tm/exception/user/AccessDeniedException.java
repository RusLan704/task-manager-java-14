package ru.bakhtiyarov.tm.exception.user;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super("Error! Access Denied...");
    }

}
