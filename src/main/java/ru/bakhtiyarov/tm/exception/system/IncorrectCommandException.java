package ru.bakhtiyarov.tm.exception.system;

public class IncorrectCommandException extends RuntimeException {

    public IncorrectCommandException(final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

}