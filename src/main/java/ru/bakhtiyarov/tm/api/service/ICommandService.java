package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
