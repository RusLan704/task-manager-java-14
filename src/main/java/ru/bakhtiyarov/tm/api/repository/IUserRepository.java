package ru.bakhtiyarov.tm.api.repository;

import ru.bakhtiyarov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User add(User user);

    User removeByLogin(String login);

    User removeById(String id);

    User removeUser(User user);

}
