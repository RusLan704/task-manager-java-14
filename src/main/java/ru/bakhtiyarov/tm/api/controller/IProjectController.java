package ru.bakhtiyarov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProject();

    void createProject();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

}
