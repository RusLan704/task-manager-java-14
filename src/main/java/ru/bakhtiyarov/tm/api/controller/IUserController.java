package ru.bakhtiyarov.tm.api.controller;

public interface IUserController {

    void updateUserPassword();

    void showUserProfile();

    void updateUserEmail();

    void updateUserFirstName();

    void updateUserLastName();

    void updateUserMiddleName();

    void updateUserLogin();

}
