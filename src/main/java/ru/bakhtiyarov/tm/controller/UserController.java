package ru.bakhtiyarov.tm.controller;

import ru.bakhtiyarov.tm.api.controller.IUserController;
import ru.bakhtiyarov.tm.api.service.IAuthService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public class UserController implements IUserController {

    private final IUserService userService;

    private final IAuthService authService;

    public UserController(IUserService userService, IAuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    private void showInfo(final User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        final String email = user.getEmail();
        if (email != null && !email.isEmpty()) System.out.println("EMAIL: " + email);
        final String firstName = user.getFirstName();
        if (firstName != null && !firstName.isEmpty()) System.out.println("FIRST NAME: " + firstName);
        final String lastName = user.getLastName();
        if (lastName != null && !lastName.isEmpty()) System.out.println("LAST NAME: " + lastName);
        final String middleName = user.getMiddleName();
        if (middleName != null && !middleName.isEmpty()) System.out.println("MIDDLE NAME: " + middleName);

    }

    @Override
    public void showUserProfile() {
        System.out.println("[SHOW USER]");
        final String userId = authService.getUserId();
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        showInfo(user);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserPassword() {
        System.out.println("[UPDATE PASSWORD]");
        final String userId = authService.getUserId();
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        User userUpdated = userService.updatePassword(userId, password);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateUserEmail() {
        System.out.println("[UPDATE EMAIL]");
        final String userId = authService.getUserId();
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User userUpdated = userService.updateUserEmail(userId, email);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateUserFirstName() {
        System.out.println("[UPDATE FIRST NAME]");
        final String userId = authService.getUserId();
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        final User userUpdated = userService.updateUserFirstName(userId, firstName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateUserLastName() {
        System.out.println("[UPDATE LAST NAME]");
        final String userId = authService.getUserId();
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        final User userUpdated = userService.updateUserLastName(userId, lastName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateUserMiddleName() {
        System.out.println("[UPDATE MIDDLE NAME]");
        final String userId = authService.getUserId();
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        final User userUpdated = userService.updateUserMiddleName(userId, middleName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateUserLogin() {
        System.out.println("[UPDATE LOGIN]");
        final String userId = authService.getUserId();
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User userUpdated = userService.updateUserLogin(userId, login);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
